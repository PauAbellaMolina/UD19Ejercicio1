import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
import javax.swing.SpringLayout;

public class mainApp extends JFrame {

	private JPanel contentPane;
	private JTextField textField;

	public static void main(String[] args) {
		
		EventQueue.invokeLater(new Runnable() {
			
			public void run() {
				
				try {
					
					mainApp frame = new mainApp();
					frame.setVisible(true);
					
				} catch (Exception e) {
					
					e.printStackTrace();
					
				}
				
			}
			
		});
		
	}

	public mainApp() {
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		SpringLayout sl_contentPane = new SpringLayout();
		contentPane.setLayout(sl_contentPane);
		
		textField = new JTextField();
		sl_contentPane.putConstraint(SpringLayout.WEST, textField, 65, SpringLayout.WEST, contentPane);
		sl_contentPane.putConstraint(SpringLayout.SOUTH, textField, -128, SpringLayout.SOUTH, contentPane);
		sl_contentPane.putConstraint(SpringLayout.EAST, textField, -59, SpringLayout.EAST, contentPane);
		textField.setHorizontalAlignment(SwingConstants.CENTER);
		contentPane.add(textField);
		textField.setColumns(10);
		
		JButton welcomeButton = new JButton("\u00A1Saludar!");
		sl_contentPane.putConstraint(SpringLayout.NORTH, welcomeButton, 47, SpringLayout.SOUTH, textField);
		sl_contentPane.putConstraint(SpringLayout.WEST, welcomeButton, 170, SpringLayout.WEST, contentPane);
		sl_contentPane.putConstraint(SpringLayout.EAST, welcomeButton, -164, SpringLayout.EAST, contentPane);
		contentPane.add(welcomeButton);
		
		JLabel textLabel = new JLabel("Escribe un nombre para saludar");
		sl_contentPane.putConstraint(SpringLayout.SOUTH, textLabel, -187, SpringLayout.SOUTH, contentPane);
		sl_contentPane.putConstraint(SpringLayout.NORTH, textField, 36, SpringLayout.SOUTH, textLabel);
		sl_contentPane.putConstraint(SpringLayout.WEST, textLabel, 0, SpringLayout.WEST, contentPane);
		sl_contentPane.putConstraint(SpringLayout.EAST, textLabel, 424, SpringLayout.WEST, contentPane);
		textLabel.setHorizontalAlignment(SwingConstants.CENTER);
		contentPane.add(textLabel);
		
		welcomeButton.addActionListener(new ActionListener() {
			
			public void actionPerformed (ActionEvent e) {
				
				JOptionPane.showMessageDialog(null, "�Hola " + textField.getText() + "!");
				
			}
			
		});
		
	}

}
